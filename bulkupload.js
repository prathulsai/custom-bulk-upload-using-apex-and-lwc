import { LightningElement,track } from 'lwc';
import saveFileForInsert from '@salesforce/apex/UploadController.saveFileForInsert';

export default class BulkUpload extends LightningElement {
    @track filesUploaded = '';
    @track fileName = '';
    @track showLoadingSpinner = false;
    @track MAX_FILE_SIZE = 1000000;
    @track fileReader = '';
    @track fileContents = '';

    handleFilesChange(event) {
        if (event.target.files.length > 0) {
            this.filesUploaded = event.target.files;
            this.fileName = event.target.files[0].name;
        }
    }
    handleSave() {
        if (this.filesUploaded.length > 0) {
            this.uploadHelper();
        } else {
            this.fileName = 'Please select a CSV file to upload!!';
        }
    }
    uploadHelper() {
        this.file = this.filesUploaded[0];
        if (this.file.size > this.MAX_FILE_SIZE) {
            window.console.log('File Size is to long');
            return;
        }
        this.showLoadingSpinner = true;
        this.fileReader = new FileReader();
        this.fileReader.onloadend = (() => {
            this.fileContents = this.fileReader.result;
            this.saveToFile();
        });
        this.fileReader.readAsText(this.file);

    }
    saveToFile() {
        saveFileForInsert({
                base64Data: JSON.stringify(this.fileContents)
            })
            .then(result => {
                this.fileName = this.fileName + ' - Uploaded Successfully';
                this.showLoadingSpinner = false;
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success!!',
                        message: this.file.name + ' - Uploaded Successfully!!!',
                        variant: 'success',
                    }),
                );
            }).catch(error => {
                this.showLoadingSpinner = false;
            });
    }
}